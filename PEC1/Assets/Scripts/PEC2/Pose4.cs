using BBUnity.Actions;
using Pada1.BBCore;
using Pada1.BBCore.Tasks;

[Action("Pose4")]
[Help("")]
public class Pose4 : GOAction
{
    public override void OnStart()
    {
        if (gameObject.GetComponent<MonitorManager>().canPose == true)
        {
            gameObject.GetComponent<MonitorManager>().DoPose(3);
        }
    }
    public override TaskStatus OnUpdate()
    {
        return TaskStatus.COMPLETED;
    }
}
