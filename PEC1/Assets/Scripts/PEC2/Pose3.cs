using BBUnity.Actions;
using Pada1.BBCore;
using Pada1.BBCore.Tasks;

[Action("Pose3")]
[Help("")]
public class Pose3 : GOAction
{
    public override void OnStart()
    {
        if (gameObject.GetComponent<MonitorManager>().canPose == true)
        {
            gameObject.GetComponent<MonitorManager>().DoPose(2);
        }
    }
    public override TaskStatus OnUpdate()
    {
        return TaskStatus.COMPLETED;
    }
}
