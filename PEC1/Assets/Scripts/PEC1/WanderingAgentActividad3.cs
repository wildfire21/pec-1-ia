using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WanderingAgentActividad3 : MonoBehaviour
{
    public UnityEngine.AI.NavMeshAgent agent;
    public float range;

    public Transform centrePoint;

    public WanderState wanderState;

    void Start()
    {
        agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
    }

    void Update()
    {
        if (agent.remainingDistance <= agent.stoppingDistance)
        {
            SetDestination();
        }

    }

    void SetDestination()
    {
        Vector3 point;
        if (RandomPoint(centrePoint.position, range, out point))
        {
            Debug.DrawRay(point, Vector3.up, Color.blue, 1.0f);
            agent.speed = Random.Range(3.5f, 5.5f);
            agent.SetDestination(point);
        }
    }

    bool RandomPoint(Vector3 center, float range, out Vector3 result)
    {
        Vector3 randomPoint = center + Random.insideUnitSphere * range;
        UnityEngine.AI.NavMeshHit hit;
        if (UnityEngine.AI.NavMesh.SamplePosition(randomPoint, out hit, 1.0f, UnityEngine.AI.NavMesh.AllAreas))
        {
            result = hit.position;
            return true;
        }

        result = Vector3.zero;
        return false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Bench") wanderState.isNearBench = true;
        else if (other.gameObject.tag == "Wanderer" || other.gameObject.tag == "Police" || other.gameObject.tag == "Thief" || other.gameObject.tag == "Taichi")
        {
            SetDestination();
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Wanderer" || other.gameObject.tag == "Police" || other.gameObject.tag == "Thief" || other.gameObject.tag == "Taichi")
        {
            SetDestination();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Bench") wanderState.isNearBench = false;
        else if (other.gameObject.tag == "Wanderer" || other.gameObject.tag == "Police" || other.gameObject.tag == "Thief" || other.gameObject.tag == "Taichi")
        {
            SetDestination();
        }
    }
    public IEnumerator Rest()
    {
        agent.speed = 0f;
        yield return new WaitForSeconds(5);
        agent.speed = Random.Range(3.5f, 5.5f);
    }
}
