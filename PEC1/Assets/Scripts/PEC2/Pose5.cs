using BBUnity.Actions;
using Pada1.BBCore;
using Pada1.BBCore.Tasks;

[Action("Pose5")]
[Help("")]
public class Pose5 : GOAction
{
    public override void OnStart()
    {
        if (gameObject.GetComponent<MonitorManager>().canPose == true)
        {
            gameObject.GetComponent<MonitorManager>().DoPose(4);
        }
    }
    public override TaskStatus OnUpdate()
    {
        return TaskStatus.COMPLETED;
    }
}
