using System.Collections;
using UnityEngine;
public class StealManager : MonoBehaviour
{
    public bool canSteal;
    public bool isStealing;
    public int stealsCount;
    void Start()
    {
        canSteal = true;
        isStealing = false;
    }
    public void DoSteal()
    {
        StartCoroutine(StealRoutine());
        ++stealsCount;
    }

    IEnumerator StealRoutine()
    {
        canSteal = false;
        isStealing = true;
        //Debug.Log("Thief number of steals: " + (stealsCount + 1));
        yield return new WaitForSeconds(2);
        canSteal = true;
        isStealing = false;
    }
}
