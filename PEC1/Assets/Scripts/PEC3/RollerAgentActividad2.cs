using UnityEngine;
using Unity.MLAgents;
using Unity.MLAgents.Sensors;
using Unity.MLAgents.Actuators;

public class RollerAgentActividad2 : Agent
{
    Rigidbody rBody;
    public Transform[] waypoints;
    void Start()
    {
        rBody = GetComponent<Rigidbody>();
    }

    public Transform Target;
    public float range;
    public override void OnEpisodeBegin()
    {
        // If the Agent fell, zero its momentum
        if (this.transform.localPosition.y < 0 || fail)
        {
            this.rBody.angularVelocity = Vector3.zero;
            this.rBody.velocity = Vector3.zero;
            this.transform.localPosition = new Vector3(0, 0.5f, 0);
            
        }

        int i = Random.Range(0, 23);

        Target.localPosition = waypoints[i].localPosition;
        //Target.localPosition = new Vector3(Random.value * 38 - 19, 1f, Random.value * 38 - 19);
    }

    public override void CollectObservations(VectorSensor sensor)
    {
        // Target and Agent positions
        sensor.AddObservation(Target.localPosition);
        sensor.AddObservation(this.transform.localPosition);

        // Agent velocity
        sensor.AddObservation(rBody.velocity.x);
        sensor.AddObservation(rBody.velocity.z);
    }

    public float forceMultiplier = 10;
    public bool reachedTarget = false;
    public bool fail = false;
    public override void OnActionReceived(ActionBuffers actionBuffers)
    {
        // Actions, size = 2
        Vector3 controlSignal = Vector3.zero;
        controlSignal.x = actionBuffers.ContinuousActions[0];
        controlSignal.z = actionBuffers.ContinuousActions[1];
        rBody.AddForce(controlSignal * forceMultiplier);

        // Rewards
        //float distanceToTarget = Vector3.Distance(this.transform.localPosition, Target.localPosition);

        // Reached target
        if (/*distanceToTarget < 1.42f*/reachedTarget)
        {
            SetReward(1.0f);
            EndEpisode();
        }

        // Fell off platform
        else if (this.transform.localPosition.y < 0)
        {
            SetReward(-2.0f);
            EndEpisode();
        }
        else if (fail)
        {
            SetReward(-1.0f);
            fail = false;
        }
        AddReward(-2.0f / MaxStep);
    }

    public override void Heuristic(in ActionBuffers actionsOut)
    {
        var continuousActionsOut = actionsOut.ContinuousActions;
        continuousActionsOut[0] = Input.GetAxis("Horizontal");
        continuousActionsOut[1] = Input.GetAxis("Vertical");
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Target") reachedTarget = true;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Target") reachedTarget = false;
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Bench" || other.gameObject.tag == "Obstacle") fail = true;
    }

    private void OnCollisionStay(Collision other)
    {
        if (other.gameObject.tag == "Bench" || other.gameObject.tag == "Obstacle") fail = true;
    }
}
