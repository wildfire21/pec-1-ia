using System.Collections;
using UnityEngine;
public class DetentionManager : MonoBehaviour
{
    public bool canDoDetention;
    public bool isDoingDetention;
    public int detentionsCount;
    void Start()
    {
        canDoDetention = true;
        isDoingDetention = false;
    }
    public void DoDetention()
    {
        StartCoroutine(DetentionRoutine());
        ++detentionsCount;
    }

    IEnumerator DetentionRoutine()
    {
        canDoDetention = false;
        isDoingDetention = true;
        //Debug.Log("Police number of detentions: " + (detentionsCount + 1));
        yield return new WaitForSeconds(2);
        canDoDetention = true;
        isDoingDetention = false;
    }
}
