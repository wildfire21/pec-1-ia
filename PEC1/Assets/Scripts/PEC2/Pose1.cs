using BBUnity.Actions;
using Pada1.BBCore;
using Pada1.BBCore.Tasks;

[Action("Pose1")]
[Help("")]
public class Pose1 : GOAction
{
    public override void OnStart()
    {
        if (gameObject.GetComponent<MonitorManager>().canPose == true)
        {
            gameObject.GetComponent<MonitorManager>().DoPose(0);
        }
    }
    public override TaskStatus OnUpdate()
    {
        return TaskStatus.COMPLETED;
    }
}
