using System.Collections;
using UnityEngine;

public class StudentManager : MonoBehaviour
{
    Renderer myRenderer;
    Renderer monitorRenderer;

    void Start()
    {
        myRenderer = GetComponent<Renderer>();
        monitorRenderer = GameObject.Find("TaichiMonitor").GetComponent<Renderer>();
    }

    public void SwitchPosition()
    {
        StartCoroutine(PoseRoutine());
    }

    IEnumerator PoseRoutine()
    {
        yield return new WaitForSeconds(0.5f);
        myRenderer.material.color = monitorRenderer.material.color;
    }
}
