using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WanderState : State
{
    public IdleState idleState;
    public bool isNearBench = false;
    public override State RunCurrentState()
    {
        if (isNearBench)
        {
            return idleState;
        }
        else
        {
            return this;
        }
    }
}
