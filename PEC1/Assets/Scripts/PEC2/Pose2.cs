using BBUnity.Actions;
using Pada1.BBCore;
using Pada1.BBCore.Tasks;

[Action("Pose2")]
[Help("")]
public class Pose2 : GOAction
{
    public override void OnStart()
    {
        if (gameObject.GetComponent<MonitorManager>().canPose == true)
        {
            gameObject.GetComponent<MonitorManager>().DoPose(1);
        }
    }
    public override TaskStatus OnUpdate()
    {
        return TaskStatus.COMPLETED;
    }
}
