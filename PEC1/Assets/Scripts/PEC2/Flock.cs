using UnityEngine;
public class Flock : MonoBehaviour
{
    float speed;
    bool turning = false;
    void Start()
    {
        speed = Random.Range(FlockManager.flockManager.minSpeed, FlockManager.flockManager.maxSpeed);
    }

    void Update()
    {
        Bounds bounds = new Bounds(FlockManager.flockManager.transform.position, FlockManager.flockManager.limits * 2);
        if (!bounds.Contains(transform.position)) turning = true;
        else turning = false;
        if (turning)
        {
            Vector3 direction = FlockManager.flockManager.transform.position - transform.position;
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction), FlockManager.flockManager.rotationSpeed * Time.deltaTime);
        }
        else
        {
            if (Random.Range(0, 100) < 10)
            {
                speed = Random.Range(FlockManager.flockManager.minSpeed, FlockManager.flockManager.maxSpeed);
            }
            if (Random.Range(0, 100) < 10)
            {
                speed = Random.Range(FlockManager.flockManager.minSpeed, FlockManager.flockManager.maxSpeed);
            }
        }
        ApplyRules();
        this.transform.Translate(0, 0, speed * Time.deltaTime);
    }
    void ApplyRules()
    {
        GameObject[] insectsGameObjects;
        insectsGameObjects = FlockManager.flockManager.allInsects;

        Vector3 centre = Vector3.zero;
        Vector3 avoid = Vector3.zero;
        float mySpeed = 0.01f;
        float distance;
        int groupSize = 0;

        foreach (GameObject go in insectsGameObjects)
        {
            if (go != this.gameObject)
            {
                distance = Vector3.Distance(go.transform.position, this.transform.position);
                if (distance <= FlockManager.flockManager.neighbourDistance)
                {
                    centre += go.transform.position;
                    groupSize++;

                    if (distance < 1.0f)
                    {
                        avoid = avoid + (this.transform.position - go.transform.position);
                    }
                    Flock anotherFlock = go.GetComponent<Flock>();
                    mySpeed = mySpeed + anotherFlock.speed;
                }
            }
        }
        if (groupSize > 0)
        {
            centre = centre / groupSize + (FlockManager.flockManager.goalPos - this.transform.position); ;
            speed = mySpeed / groupSize;
            if (speed > FlockManager.flockManager.maxSpeed) speed = FlockManager.flockManager.maxSpeed;
            Vector3 direction = (centre + avoid) - transform.position;
            if (direction != Vector3.zero) transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction), FlockManager.flockManager.rotationSpeed * Time.deltaTime);
        }
    }
}
