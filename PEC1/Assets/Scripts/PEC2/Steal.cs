using BBUnity.Actions;
using Pada1.BBCore;
using Pada1.BBCore.Tasks;

[Action("Steal")]
[Help("")]
public class Steal : GOAction
{
    public override TaskStatus OnUpdate()
    {
        if (gameObject.GetComponent<StealManager>().canSteal == true)
        {
            gameObject.GetComponent<StealManager>().DoSteal();
        }
        return TaskStatus.RUNNING;
    }
}
