using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlockManager : MonoBehaviour
{
    public static FlockManager flockManager;
    public GameObject insectPrefab;
    public int instectsNum = 20;
    public GameObject[] allInsects;
    public Vector3 limits = new Vector3(0f, 0f, 0f);
    public Vector3 goalPos = Vector3.zero;

    [Range(0.0f, 5.0f)]
    public float minSpeed;
    [Range(0.0f, 5.0f)]
    public float maxSpeed;
    [Range(1.0f, 10.0f)]
    public float neighbourDistance;
    [Range(1.0f, 5.0f)]
    public float rotationSpeed;

    void Start()
    {
        allInsects = new GameObject[instectsNum];
        
        for(int i = 0; i < instectsNum; ++i)
        {
            Vector3 position = this.transform.position + new Vector3(Random.Range(-limits.x, limits.x), Random.Range(-limits.y, limits.y), Random.Range(-limits.z, limits.z));
            allInsects[i] = Instantiate(insectPrefab, position, Quaternion.identity);
        }

        flockManager = this;
        goalPos = this.transform.position;
    }

    void Update()
    {
        if (Random.Range(0, 100) < 2) goalPos = this.transform.position + new Vector3(Random.Range(-limits.x, limits.x), Random.Range(-limits.y, limits.y), Random.Range(-limits.z, limits.z));
    }
}
