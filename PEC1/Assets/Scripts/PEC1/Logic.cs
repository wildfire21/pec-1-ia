using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Logic : MonoBehaviour
{
    public NavMeshAgent player;
    private NavMeshAgent persecutor;

    private void Start()
    {
        persecutor = GameObject.Find("Persecutor").GetComponent<NavMeshAgent>();
    }
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(camRay, out hit, 100))
            {
                player.destination = hit.point;
            }
        }
        persecutor.destination = player.transform.position;
    }
}
