using BBUnity.Actions;
using Pada1.BBCore;
using Pada1.BBCore.Tasks;

[Action("Detention")]
[Help("")]
public class Detention : GOAction
{
    public override TaskStatus OnUpdate()
    {
        if (gameObject.GetComponent<DetentionManager>().canDoDetention == true)
        {
            gameObject.GetComponent<DetentionManager>().DoDetention();
        }
        return TaskStatus.RUNNING;
    }
}
