using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdleState : State
{
    public WanderingAgentActividad3 wanderingAgent;
    public override State RunCurrentState()
    {
        if (wanderingAgent.wanderState.isNearBench)
        {
            StartCoroutine(wanderingAgent.Rest());
            return this;
        }
        else return wanderingAgent.wanderState;
    }
}
