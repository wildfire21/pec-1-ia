using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonitorManager : MonoBehaviour
{
    public bool canPose;
    public bool isPosing;
    public Material[] materials;
    new Renderer renderer;
    public List<GameObject> students = new List<GameObject>();

    void Start()
    {
        canPose = true;
        isPosing = false;

        renderer = GetComponent<Renderer>();
        materials[0].color = Color.red;
        materials[1].color = Color.yellow;
        materials[2].color = Color.green;
        materials[3].color = Color.blue;
        materials[4].color = Color.black;
    }

    public void DoPose(int i)
    {
        StartCoroutine(PoseRoutine(i));
    }

    IEnumerator PoseRoutine(int i)
    {
        canPose = false;
        isPosing = true;
        //Debug.Log("Pose" + i);
        renderer.material = materials[i];
        TeachStudents();
        yield return new WaitForSeconds(2);
        canPose = true;
        isPosing = false;
    }

    public void TeachStudents()
    {
        for (int i = 0; i < students.Count; i++)
        {
            students[i].GetComponent<StudentManager>().SwitchPosition();
        }
    }
}
